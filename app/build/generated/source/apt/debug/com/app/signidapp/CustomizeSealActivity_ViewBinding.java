// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.app.signidapp.font.ArialRegularEditText;
import com.app.signidapp.font.GothamMediumButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CustomizeSealActivity_ViewBinding implements Unbinder {
  private CustomizeSealActivity target;

  private View view2131230817;

  private View view2131230816;

  private View view2131230807;

  @UiThread
  public CustomizeSealActivity_ViewBinding(CustomizeSealActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CustomizeSealActivity_ViewBinding(final CustomizeSealActivity target, View source) {
    this.target = target;

    View view;
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
    view = Utils.findRequiredView(source, R.id.btnSave, "field 'btnSave' and method 'onClick'");
    target.btnSave = Utils.castView(view, R.id.btnSave, "field 'btnSave'", GothamMediumButton.class);
    view2131230817 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progress_bar, "field 'progressBar'", ProgressBar.class);
    target.etName = Utils.findRequiredViewAsType(source, R.id.etName, "field 'etName'", ArialRegularEditText.class);
    target.etEmail = Utils.findRequiredViewAsType(source, R.id.etEmail, "field 'etEmail'", ArialRegularEditText.class);
    view = Utils.findRequiredView(source, R.id.btnPreview, "field 'btnPreview' and method 'onClick'");
    target.btnPreview = Utils.castView(view, R.id.btnPreview, "field 'btnPreview'", GothamMediumButton.class);
    view2131230816 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.backBtn, "field 'backBtn' and method 'onClick'");
    target.backBtn = Utils.castView(view, R.id.backBtn, "field 'backBtn'", ImageView.class);
    view2131230807 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CustomizeSealActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerView = null;
    target.btnSave = null;
    target.progressBar = null;
    target.etName = null;
    target.etEmail = null;
    target.btnPreview = null;
    target.backBtn = null;

    view2131230817.setOnClickListener(null);
    view2131230817 = null;
    view2131230816.setOnClickListener(null);
    view2131230816 = null;
    view2131230807.setOnClickListener(null);
    view2131230807 = null;
  }
}
