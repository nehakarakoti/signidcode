// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.signidapp.font.MyriadProTextView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(MainActivity target, View source) {
    this.target = target;

    target.mainViewPager = Utils.findRequiredViewAsType(source, R.id.mainViewPager, "field 'mainViewPager'", ViewPager.class);
    target.viewPagerCountDots = Utils.findRequiredViewAsType(source, R.id.viewPagerCountDots, "field 'viewPagerCountDots'", LinearLayout.class);
    target.viewPagerIndicator = Utils.findRequiredViewAsType(source, R.id.viewPagerIndicator, "field 'viewPagerIndicator'", RelativeLayout.class);
    target.textGetStarted = Utils.findRequiredViewAsType(source, R.id.text_get_started, "field 'textGetStarted'", MyriadProTextView.class);
    target.LL_get_started_container = Utils.findRequiredViewAsType(source, R.id.LL_get_started_container, "field 'LL_get_started_container'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mainViewPager = null;
    target.viewPagerCountDots = null;
    target.viewPagerIndicator = null;
    target.textGetStarted = null;
    target.LL_get_started_container = null;
  }
}
