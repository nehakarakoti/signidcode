// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GoActivity_ViewBinding implements Unbinder {
  private GoActivity target;

  @UiThread
  public GoActivity_ViewBinding(GoActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GoActivity_ViewBinding(GoActivity target, View source) {
    this.target = target;

    target.text_got_it = Utils.findRequiredViewAsType(source, R.id.text_got_it, "field 'text_got_it'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GoActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text_got_it = null;
  }
}
