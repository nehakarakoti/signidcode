// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.app.signidapp.utils.ImageViewSealDetails;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SealDetailActivity_ViewBinding implements Unbinder {
  private SealDetailActivity target;

  private View view2131230807;

  @UiThread
  public SealDetailActivity_ViewBinding(SealDetailActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SealDetailActivity_ViewBinding(final SealDetailActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.backBtn, "field 'backBtn' and method 'onClick'");
    target.backBtn = Utils.castView(view, R.id.backBtn, "field 'backBtn'", ImageView.class);
    view2131230807 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
    target.ivName = Utils.findRequiredViewAsType(source, R.id.ivName, "field 'ivName'", ImageViewSealDetails.class);
    target.ivSeal = Utils.findRequiredViewAsType(source, R.id.ivSeal, "field 'ivSeal'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SealDetailActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.ivName = null;
    target.ivSeal = null;

    view2131230807.setOnClickListener(null);
    view2131230807 = null;
  }
}
