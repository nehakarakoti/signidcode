// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.signidapp.font.GothamMediumButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignInActivity_ViewBinding implements Unbinder {
  private SignInActivity target;

  @UiThread
  public SignInActivity_ViewBinding(SignInActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignInActivity_ViewBinding(SignInActivity target, View source) {
    this.target = target;

    target.LL_bottom_container = Utils.findRequiredViewAsType(source, R.id.LL_bottom_container, "field 'LL_bottom_container'", LinearLayout.class);
    target.LLLogoContainer = Utils.findRequiredViewAsType(source, R.id.LL_logo_container, "field 'LLLogoContainer'", LinearLayout.class);
    target.inputEmail = Utils.findRequiredViewAsType(source, R.id.inputEmail, "field 'inputEmail'", EditText.class);
    target.LLEmailContainer = Utils.findRequiredViewAsType(source, R.id.LL_email_container, "field 'LLEmailContainer'", LinearLayout.class);
    target.inputPass = Utils.findRequiredViewAsType(source, R.id.inputPass, "field 'inputPass'", EditText.class);
    target.LLPasswordContainer = Utils.findRequiredViewAsType(source, R.id.LL_password_container, "field 'LLPasswordContainer'", LinearLayout.class);
    target.LLLoginContainer = Utils.findRequiredViewAsType(source, R.id.LL_login_container, "field 'LLLoginContainer'", LinearLayout.class);
    target.LLForgotPasswordContainer = Utils.findRequiredViewAsType(source, R.id.LL_forgotPasswordContainer, "field 'LLForgotPasswordContainer'", LinearLayout.class);
    target.signInBtn = Utils.findRequiredViewAsType(source, R.id.sign_in_btn, "field 'signInBtn'", GothamMediumButton.class);
    target.LLSignInButtonContainer = Utils.findRequiredViewAsType(source, R.id.LL_sign_in_button_container, "field 'LLSignInButtonContainer'", LinearLayout.class);
    target.LL_signUp = Utils.findRequiredViewAsType(source, R.id.LL_signUp, "field 'LL_signUp'", LinearLayout.class);
    target.textForgotPassword = Utils.findRequiredViewAsType(source, R.id.textForgotPassword, "field 'textForgotPassword'", TextView.class);
    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SignInActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.LL_bottom_container = null;
    target.LLLogoContainer = null;
    target.inputEmail = null;
    target.LLEmailContainer = null;
    target.inputPass = null;
    target.LLPasswordContainer = null;
    target.LLLoginContainer = null;
    target.LLForgotPasswordContainer = null;
    target.signInBtn = null;
    target.LLSignInButtonContainer = null;
    target.LL_signUp = null;
    target.textForgotPassword = null;
    target.backBtn = null;
  }
}
