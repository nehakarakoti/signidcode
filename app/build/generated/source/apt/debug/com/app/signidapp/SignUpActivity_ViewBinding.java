// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.signidapp.font.GothamMediumButton;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUpActivity_ViewBinding implements Unbinder {
  private SignUpActivity target;

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignUpActivity_ViewBinding(SignUpActivity target, View source) {
    this.target = target;

    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.inputUserName = Utils.findRequiredViewAsType(source, R.id.inputUserName, "field 'inputUserName'", EditText.class);
    target.LLUsernameContainer = Utils.findRequiredViewAsType(source, R.id.LL_username_container, "field 'LLUsernameContainer'", LinearLayout.class);
    target.inputEmail = Utils.findRequiredViewAsType(source, R.id.inputEmail, "field 'inputEmail'", EditText.class);
    target.LLEmailContainer = Utils.findRequiredViewAsType(source, R.id.LL_email_container, "field 'LLEmailContainer'", LinearLayout.class);
    target.inputPass = Utils.findRequiredViewAsType(source, R.id.inputPass, "field 'inputPass'", EditText.class);
    target.LLPassContainer = Utils.findRequiredViewAsType(source, R.id.LL_pass_container, "field 'LLPassContainer'", LinearLayout.class);
    target.inputCPass = Utils.findRequiredViewAsType(source, R.id.inputCPass, "field 'inputCPass'", EditText.class);
    target.LLCpassContainer = Utils.findRequiredViewAsType(source, R.id.LL_cpass_container, "field 'LLCpassContainer'", LinearLayout.class);
    target.LLSignupPanelContainer = Utils.findRequiredViewAsType(source, R.id.LL_signupPanelContainer, "field 'LLSignupPanelContainer'", LinearLayout.class);
    target.LLContainer = Utils.findRequiredViewAsType(source, R.id.LL_container, "field 'LLContainer'", LinearLayout.class);
    target.btnSignUp = Utils.findRequiredViewAsType(source, R.id.btn_sign_up, "field 'btnSignUp'", GothamMediumButton.class);
    target.LLSignUpButtonContianer = Utils.findRequiredViewAsType(source, R.id.LL_signUp_button_contianer, "field 'LLSignUpButtonContianer'", LinearLayout.class);
    target.LLSignupInfoContainer = Utils.findRequiredViewAsType(source, R.id.LL_signup_info_container, "field 'LLSignupInfoContainer'", LinearLayout.class);
    target.LL_Toc_container = Utils.findRequiredViewAsType(source, R.id.LL_Toc_container, "field 'LL_Toc_container'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SignUpActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.inputUserName = null;
    target.LLUsernameContainer = null;
    target.inputEmail = null;
    target.LLEmailContainer = null;
    target.inputPass = null;
    target.LLPassContainer = null;
    target.inputCPass = null;
    target.LLCpassContainer = null;
    target.LLSignupPanelContainer = null;
    target.LLContainer = null;
    target.btnSignUp = null;
    target.LLSignUpButtonContianer = null;
    target.LLSignupInfoContainer = null;
    target.LL_Toc_container = null;
  }
}
