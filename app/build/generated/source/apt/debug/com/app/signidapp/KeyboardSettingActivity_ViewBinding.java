// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class KeyboardSettingActivity_ViewBinding implements Unbinder {
  private KeyboardSettingActivity target;

  @UiThread
  public KeyboardSettingActivity_ViewBinding(KeyboardSettingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public KeyboardSettingActivity_ViewBinding(KeyboardSettingActivity target, View source) {
    this.target = target;

    target.add_keyboard_btn = Utils.findRequiredViewAsType(source, R.id.add_keyboard_btn, "field 'add_keyboard_btn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    KeyboardSettingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.add_keyboard_btn = null;
  }
}
