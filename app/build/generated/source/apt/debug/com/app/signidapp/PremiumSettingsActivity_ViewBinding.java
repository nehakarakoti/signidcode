// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PremiumSettingsActivity_ViewBinding implements Unbinder {
  private PremiumSettingsActivity target;

  private View view2131230807;

  @UiThread
  public PremiumSettingsActivity_ViewBinding(PremiumSettingsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public PremiumSettingsActivity_ViewBinding(final PremiumSettingsActivity target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.backBtn, "field 'backBtn' and method 'onClick'");
    target.backBtn = Utils.castView(view, R.id.backBtn, "field 'backBtn'", ImageView.class);
    view2131230807 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick();
      }
    });
    target.recyclerView = Utils.findRequiredViewAsType(source, R.id.recyclerView, "field 'recyclerView'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PremiumSettingsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.recyclerView = null;

    view2131230807.setOnClickListener(null);
    view2131230807 = null;
  }
}
