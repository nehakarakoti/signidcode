// Generated code from Butter Knife. Do not modify!
package com.app.signidapp.utils;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.app.signidapp.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class StampTrialActivity_ViewBinding implements Unbinder {
  private StampTrialActivity target;

  @UiThread
  public StampTrialActivity_ViewBinding(StampTrialActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public StampTrialActivity_ViewBinding(StampTrialActivity target, View source) {
    this.target = target;

    target.ivImage = Utils.findRequiredViewAsType(source, R.id.ivImage, "field 'ivImage'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    StampTrialActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivImage = null;
  }
}
