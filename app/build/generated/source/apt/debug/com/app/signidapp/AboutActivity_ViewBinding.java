// Generated code from Butter Knife. Do not modify!
package com.app.signidapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class AboutActivity_ViewBinding implements Unbinder {
  private AboutActivity target;

  @UiThread
  public AboutActivity_ViewBinding(AboutActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public AboutActivity_ViewBinding(AboutActivity target, View source) {
    this.target = target;

    target.backBtn = Utils.findRequiredViewAsType(source, R.id.backBtn, "field 'backBtn'", ImageView.class);
    target.LL_privacy_policy_container = Utils.findRequiredViewAsType(source, R.id.LL_privacy_policy_container, "field 'LL_privacy_policy_container'", LinearLayout.class);
    target.LL_TUC_container = Utils.findRequiredViewAsType(source, R.id.LL_TUC_container, "field 'LL_TUC_container'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    AboutActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.backBtn = null;
    target.LL_privacy_policy_container = null;
    target.LL_TUC_container = null;
  }
}
