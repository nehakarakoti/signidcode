package com.app.signidapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.signidapp.CustomizeSealActivity;
import com.app.signidapp.R;
import com.app.signidapp.SealDetailActivity;
import com.app.signidapp.model.SealInfo;
import com.app.signidapp.utils.GraphBelowsmallImageview;
import com.app.signidapp.utils.GraphShorterCircleName;
import com.bumptech.glide.Glide;

import java.util.List;

public class PremiumScreenAdapter extends RecyclerView.Adapter {
    List<SealInfo> mlist;
    Context mContext;
    LayoutInflater mLayout;
    private int circleadd = 1, normalpictures = 2;

    public PremiumScreenAdapter(List<SealInfo> mlist, Context mContext) {
        this.mlist = mlist;
        this.mContext = mContext;
        mLayout = LayoutInflater.from(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == circleadd)
            return new AddViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.recycleradd, parent, false));

        else

            return new SealOptions(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.recycleritem2, parent, false));
//        else
//        new GraphicsView(mContext);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder.getItemViewType() == normalpictures) {
            SealInfo s = mlist.get(position);
            SealOptions squadInfoViewHolder = (SealOptions) holder;

       if (s.getImage().equals("http://signidapp.com/SignId/SealDocuments/21.png")) {

                squadInfoViewHolder.ivNameShort.changeText(s.getName());

                squadInfoViewHolder.ivNameShort.changeTextsize(12f);
                squadInfoViewHolder.ivNameShort.changeText(s.getName());
                squadInfoViewHolder.ivNameShort.changeColor(true);

                squadInfoViewHolder.ivCitySamll.changeText(s.getTown());
                squadInfoViewHolder.ivCitySamll.changeTextsize(12f);
                squadInfoViewHolder.ivCitySamll.changeColor(true);
                Glide.with(mContext).load(
                        s.getImage())
                        .into(squadInfoViewHolder.rohumbusStamp);

                squadInfoViewHolder.ivCitySamll.setVisibility(View.VISIBLE);
            }
            squadInfoViewHolder.ivNameShort.changeText(s.getName());

            squadInfoViewHolder.ivNameShort.changeTextsize(12f);
            squadInfoViewHolder.ivNameShort.changeText(s.getName());
            squadInfoViewHolder.ivNameShort.changeColor(true);

            squadInfoViewHolder.ivCitySamll.changeText(s.getTown());
            squadInfoViewHolder.ivCitySamll.changeTextsize(12f);
            squadInfoViewHolder.ivCitySamll.changeColor(true);
            Glide.with(mContext).load(
                    s.getImage())
                    .into(squadInfoViewHolder.rohumbusStamp);

            squadInfoViewHolder.ivCitySamll.setVisibility(View.VISIBLE);

        }


    }


    @Override
    public int getItemCount() {
        return mlist.size();
    }

    public int getItemViewType(int position) {
        if (position == 0) {

            return circleadd;

        } else
            return normalpictures;
    }

    public class AddViewHolder extends RecyclerView.ViewHolder {
        ImageView ivAdd;


        public AddViewHolder(View itemView) {
            super(itemView);
            ivAdd = itemView.findViewById(R.id.ivAdd);

            ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.startActivity(new Intent(mContext, CustomizeSealActivity.class));
                }
            });
        }
    }

    public class SealOptions extends RecyclerView.ViewHolder {


        GraphBelowsmallImageview ivCitySamll;
        ImageView rohumbusStamp;
        GraphShorterCircleName ivNameShort;

        public SealOptions(View itemView) {
            super(itemView);

            ivNameShort = itemView.findViewById(R.id.ivNameShort);
            ivCitySamll = itemView.findViewById(R.id.ivCitySmall);

            rohumbusStamp = itemView.findViewById(R.id.rohumbusStamp);

            rohumbusStamp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mContext, SealDetailActivity.class);
                    SealInfo s = mlist.get(getAdapterPosition());

                    i.putExtra("SealInfo", s);
                    //   i.putExtra("url",mlist.get(getAdapterPosition()).getImage());
                    mContext.startActivity(i);
                }
            });
        }
    }

}

