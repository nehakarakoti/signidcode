package com.app.signidapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.app.signidapp.R;
import com.app.signidapp.model.ImageData;
import com.app.signidapp.reciever.ImageInterface;
import com.bumptech.glide.Glide;

import java.util.List;

public class CustomizeSealAdapter extends RecyclerView.Adapter {
    Context mContext;
    List<String> mList;
    List<ImageData> imageData;
    LayoutInflater mLayout;
    ImageInterface imageInterface;

    public CustomizeSealAdapter(Context mContext, List<String> mList, ImageInterface imageInterface, List<ImageData> imageData) {
        this.mContext = mContext;
        this.imageInterface = imageInterface;
        this.mList = mList;
        this.imageData = imageData;
        mLayout = LayoutInflater.from(mContext);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyView(LayoutInflater.from(mContext).inflate(R.layout.recyclerview_image, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        MyView myView = (MyView) holder;
        ImageData i = imageData.get(position);
        String s = imageData.get(position).getUrl();
        Glide.with(mContext).load(
                s)
                .into(myView.ivImage);
        if (i.isChecked())
            myView.ivTick.setVisibility(View.VISIBLE);
        else
            myView.ivTick.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return imageData.size();
    }

    public class MyView extends RecyclerView.ViewHolder {
        ImageView ivImage, ivTick;
        RelativeLayout rlMainLayout;

        public MyView(View itemView) {
            super(itemView);
            ivTick = itemView.findViewById(R.id.ivTick);
            ivImage = itemView.findViewById(R.id.ivImage);
            rlMainLayout = itemView.findViewById(R.id.rlMainLayout);
            ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int j = 0; j < imageData.size(); j++) {
                        if (j == getAdapterPosition()) {
                            imageData.get(getAdapterPosition()).setChecked(true);
                            imageInterface.showingImage(imageData.get(j).getImage_ID());
                        } else
                            imageData.get(j).setChecked(false);

                    }
                    notifyDataSetChanged();
                }

            });
        }
    }
}
