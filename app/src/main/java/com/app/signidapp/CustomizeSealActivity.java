package com.app.signidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.signidapp.adapter.CustomizeSealAdapter;
import com.app.signidapp.font.ArialRegularEditText;
import com.app.signidapp.font.GothamMediumButton;
import com.app.signidapp.model.ImageData;
import com.app.signidapp.model.SealInfo;
import com.app.signidapp.reciever.ImageInterface;
import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.app.signidapp.utils.CommonValidationDialog;
import com.app.signidapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomizeSealActivity extends AppCompatActivity {
    List<String> mList = new ArrayList<>();
    List<String> idList = new ArrayList<>();
    List<ImageData> imageLists = new ArrayList<>();
    Activity mActivity = CustomizeSealActivity.this;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.btnSave)
    GothamMediumButton btnSave;
    CustomizeSealAdapter cAdapter;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.etName)
    ArialRegularEditText etName;
    @BindView(R.id.etEmail)
    ArialRegularEditText etEmail;


    @BindView(R.id.btnPreview)
    GothamMediumButton btnPreview;
    @BindView(R.id.backBtn)
    ImageView backBtn;
    String SealSelected = "";
    ImageInterface imageInterface = new ImageInterface() {
        @Override
        public void showingImage(String s) {
            if (s != null) {
                SealSelected = s;

            }
        }

        @Override
        public void getImagetobeShown() {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customize_seal);
        ButterKnife.bind(this);
        cAdapter = new CustomizeSealAdapter(CustomizeSealActivity.this, mList, imageInterface, imageLists);

        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(CustomizeSealActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(cAdapter);
        if (isNetworkAvailable(CustomizeSealActivity.this))
            SealApiHitting();
        else
            Toast.makeText(CustomizeSealActivity.this, getString(R.string.checkNetwork), Toast.LENGTH_LONG).show();
    }

    private void SealApiHitting() {
        progressBar.setVisibility(View.VISIBLE);
        try {

            String url = "http://www.signidapp.com/SignId/All_Seal_Images.php";


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            progressBar.setVisibility(View.GONE);


                            System.out.println("RESPONSE=====" + response);


                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (!jsonObject.isNull("All Seals")) {
                                    JSONArray mJsonArray = jsonObject.getJSONArray("All Seals");

                                    for (int i = 0; i < mJsonArray.length(); i++) {
                                        JSONObject mJsonData = mJsonArray.getJSONObject(i);
                                        mList.add(mJsonData.getString("image"));
                                        idList.add(mJsonData.getString("image_id"));
                                        Log.i("nehahdjfs", mList.get(i));
                                    }

                                }
                                for (int i = 0; i < mList.size(); i++) {
                                    imageLists.add(new ImageData(mList.get(i), false, idList.get(i)));
                                }

                                cAdapter.notifyDataSetChanged();

                            } catch (Exception ex) {

                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressBar.setVisibility(View.GONE);
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CustomizeSealActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }

    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    @OnClick({R.id.btnSave, R.id.btnPreview, R.id.backBtn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSave:
                if (etName.getText().toString().equals(""))
                    CommonValidationDialog.showLoadingDialog(CustomizeSealActivity.this, getString(R.string.enterNameCheck));
                else if (etEmail.getText().toString().equals(""))

                    CommonValidationDialog.showLoadingDialog(CustomizeSealActivity.this, getString(R.string.leave));
                else
                    apiHitting();

                break;
            case R.id.btnPreview:

                if (SealSelected.equals("")) {
                    CommonValidationDialog.showLoadingDialog(CustomizeSealActivity.this, getString(R.string.selectSeal));

                } else if (etName.getText().toString().trim().equals("")) {
                    CommonValidationDialog.showLoadingDialog(CustomizeSealActivity.this, getString(R.string.enterNameCheck));
                } else if (etEmail.getText().toString().trim().equals(""))

                    CommonValidationDialog.showLoadingDialog(CustomizeSealActivity.this, getString(R.string.enterEnterCheck));


                else {
                    SealInfo s = new SealInfo(SealSelected, etName.getText().toString().trim(), etEmail.getText().toString().trim());
                    Intent i = new Intent(mActivity, SealDetailActivity.class);
                    i.putExtra("SealInforObject", s);
                    startActivity(i);

                }


                break;
            case R.id.backBtn:
                finish();
        }
    }

    private void apiHitting() {
        String user_id = SignIDAppSession.readString(mActivity, SignIDAppSession.KEY_USER_ID, "");

        try {
            String url = Content.baseURL + "Add_Seal.php?user_id=" + user_id + "&image_id=" + SealSelected + "&name=" + etName.getText().toString().trim() + "&town=" + etEmail.getText().toString().trim();

            System.out.println("URL===" + url);


            CommonLoadingDialog.showLoadingDialog(CustomizeSealActivity.this, "Log In..");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("RESPONSE=====" + response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("Status");
                                String message = jsonObject.getString("Message");

                                if (status.equals("1")) {
                                    finish();
//                                    Gson gson = new Gson();
//                                    SignInResponse signInResponse = gson.fromJson(response, SignInResponse.class);
//                                    String user_id = signInResponse.getData().getUser_id();
//                                    String email_id = signInResponse.getData().getEmail();
//                                    String user_name = signInResponse.getData().getUsername();
//                                    Toast.makeText(CustomizeSealActivity.this, message, Toast.LENGTH_SHORT).show();
                                     // showLoginSuccess("Login successful");
                               } else {
                                    String message1 = jsonObject.getString("message");
                                    if (message1.equals("Username or Password may be wrong ")) {
                                        //showLoginFails("Please make sure your entered valid Username or Password ");

                                    } else {
                                        //      showLoginFails("Login fail!make sure you have verified your credentials first");

                                    }

                                }


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(CustomizeSealActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());

        }

    }


}
