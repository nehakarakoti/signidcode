package com.app.signidapp.utils;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.app.signidapp.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StampTrialActivity extends AppCompatActivity {

    @BindView(R.id.ivImage)
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stamp_trial);
        ButterKnife.bind(this);
        Glide.with(StampTrialActivity.this).load(
               "http://signidapp.com/SignId/SealDocuments/22.png")
                .into(ivImage);
    }

}
