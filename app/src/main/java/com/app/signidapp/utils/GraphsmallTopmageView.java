package com.app.signidapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import com.app.signidapp.R;
import com.app.signidapp.font.GothamMediumTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class GraphsmallTopmageView extends GothamMediumTextView {
    Float f = 13f;
    Boolean colorChange = false;
    private String QUOTE = "                                                    Neha karakotisdgsg";
    private Path circle, circle2;
    private Paint mCirlcePaint;
    private Paint tPaint;
    private Rect textBounds;
    private int mTextWidth, mTextHeight, centerX, centerY;
    private float radius;

    public GraphsmallTopmageView(Context context, String qu) {
        super(context);
        this.QUOTE = qu;
    }

    public GraphsmallTopmageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphsmallTopmageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    public void changeColor(Boolean i) {
        this.colorChange = i;
    }

    public void changeTextsize(Float f) {
        this.f = f;
    }

    public void changeText(String s) {
        this.QUOTE = s;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        circle = new Path();

        tPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        if (colorChange)
            tPaint.setColor(Color.WHITE);
        else
            tPaint.setColor(Color.BLACK);
        tPaint.setTextSize(f);

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/constanb.ttf");
        tPaint.setTypeface(font);
        tPaint.setTextAlign(Paint.Align.CENTER);
        textBounds = new Rect();
        tPaint.getTextBounds(QUOTE, 0, QUOTE.length(), textBounds);
        mTextWidth = Math.round(tPaint.measureText(QUOTE.toString())); // Use measureText to calculate width
        mTextHeight = textBounds.height(); // Use height from getTextBounds()
        mCirlcePaint = new Paint();
        mCirlcePaint.setStyle(Paint.Style.FILL);

        mCirlcePaint.setColor(Color.TRANSPARENT);
        if (QUOTE.length() < 8) {
            radius = getResources().getDimension(R.dimen._10sdp);
            centerX = getWidth() / 2;
            centerY = getHeight() / 2;
            canvas.rotate(getResources().getDimension(R.dimen._180sdp), centerX, centerY);
            canvas.drawCircle(centerX, centerY, radius, mCirlcePaint);
            circle.addCircle(centerX, centerY, radius, Path.Direction.CW);
    //back upgetResources().getDimension(R.dimen._40sdp)

            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                canvas.drawTextOnPath(QUOTE, circle,getResources().getDimension(R.dimen._35sdp), 30, tPaint);
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                canvas.drawTextOnPath(QUOTE, circle, getResources().getDimension(R.dimen._35sdp), 30, tPaint);
            } else
                canvas.drawTextOnPath(QUOTE, circle,getResources().getDimension(R.dimen._35sdp), 30, tPaint);


        } else if (QUOTE.length() < 19) {
//70//
            radius = getResources().getDimension(R.dimen._24sdp);
            centerX = getWidth() / 2;
            centerY = getHeight() / 2;
            canvas.rotate(getResources().getDimension(R.dimen._180sdp), centerX, centerY);
            canvas.drawCircle(centerX, centerY, radius, mCirlcePaint);
            circle.addCircle(centerX, centerY, radius, Path.Direction.CW);



            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
                canvas.drawTextOnPath(QUOTE, circle,getResources().getDimension(R.dimen._30sdp), 0, tPaint);
            } else if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
                canvas.drawTextOnPath(QUOTE, circle, getResources().getDimension(R.dimen._30sdp), 0, tPaint);
            } else
                canvas.drawTextOnPath(QUOTE, circle,getResources().getDimension(R.dimen._30sdp), 0, tPaint);

        } else {

        }


        //11070
        super.onDraw(canvas);
        //worked
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);


    }
}
