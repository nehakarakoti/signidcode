package com.app.signidapp.utils;

import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodSubtype;

/**
 * Created by ankit on 4/4/16.
 */
public class LatinKeyboardView extends KeyboardView {
    static final int KEYCODE_OPTIONS = -100;
    static final int KEYCODE_LANGUAGE_SWITCH = -101;
    static final int KEYCODE_API = -600;

    public LatinKeyboardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatinKeyboardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected boolean onLongPress(Key key) {
//        if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
//            getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
//            return true;
//        } else {
//
            if (key.codes[0] == Keyboard.KEYCODE_CANCEL) {
                getOnKeyboardActionListener().onKey(KEYCODE_OPTIONS, null);
                return true;
            } else if (key.codes[0] == '0') {
                getOnKeyboardActionListener().onKey('+', null);
                return true;
            } else if (key.codes[0] == 'q' || key.codes[0] == 'Q' || key.codes[0] == '۱') {
                getOnKeyboardActionListener().onKey('1', null);
                return true;
            } else if (((key.codes[0] == 'w' || key.codes[0] == 'W') ) || key.codes[0] == '۲') {
                getOnKeyboardActionListener().onKey('2', null);
                return true;
            } else if (key.codes[0] == 'e' || key.codes[0] == 'E' || key.codes[0] == '۳') {
                getOnKeyboardActionListener().onKey('3', null);
                return true;
            } else if (((key.codes[0] == 'r' || key.codes[0] == 'R')) || key.codes[0] == '۴') {
                getOnKeyboardActionListener().onKey('4', null);
                return true;
            } else if (((key.codes[0] == 't' || key.codes[0] == 'T')) || key.codes[0] == '۵') {
                getOnKeyboardActionListener().onKey('5', null);
                return true;
            } else if (key.codes[0] == 'y' || key.codes[0] == 'Y' || key.codes[0] == '۶') {
                getOnKeyboardActionListener().onKey('6', null);
                return true;
            } else if (key.codes[0] == 'u' || key.codes[0] == 'U' || key.codes[0] == '۷') {
                getOnKeyboardActionListener().onKey('7', null);
                return true;
            } else if (key.codes[0] == 'i' || key.codes[0] == 'I' || key.codes[0] == '۸') {
                getOnKeyboardActionListener().onKey('8', null);
                return true;
            } else if (key.codes[0] == 'o' || key.codes[0] == 'O' || key.codes[0] == '۹') {
                getOnKeyboardActionListener().onKey('9', null);
                return true;
            } else if (key.codes[0] == 'p' || key.codes[0] == 'P' || key.codes[0] == '۰') {
                getOnKeyboardActionListener().onKey('0', null);
                return true;
            } else if ((key.codes[0] == 'S' || key.codes[0] == 's') ) {
                getOnKeyboardActionListener().onKey('ϐ', null);
                return true;}
           // return super.onLongPress(key);
      //  }
        else {
                 return super.onLongPress(key);
            }

    }

    void setSubtypeOnSpaceKey(final InputMethodSubtype subtype) {
        final LatinKeyboard keyboard = (LatinKeyboard) getKeyboard();
        // keyboard.setSpaceIcon(getResources().getDrawable(subtype.getIconResId()));
        // keyboard.setShifted(true);
        invalidateAllKeys();
    }
}
