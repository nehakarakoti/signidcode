package com.app.signidapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;

import com.app.signidapp.R;
import com.app.signidapp.font.GothamMediumTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class GraphBelowsmallImageview  extends GothamMediumTextView {
    private String QUOTE ="Chandigarhfsf";
    private Path circle,circle2;
    private Paint mCirlcePaint;
    private Paint tPaint;
    private Rect textBounds;
    private int mTextWidth, mTextHeight, centerX, centerY;
    private float radius;
    boolean colorChange=false;
    private float size=10f;
    public void changeText(String s){
        this.QUOTE=s;
    }
    public void changeColor(Boolean i){
        this.colorChange=i;
    }

    public GraphBelowsmallImageview(Context context,String s) {
        super(context);
        this.QUOTE = s;
    }

    public GraphBelowsmallImageview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GraphBelowsmallImageview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        circle = new Path();
        tPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        if(colorChange)
            tPaint.setColor(Color.WHITE);
        else
            tPaint.setColor(Color.BLACK);

        tPaint.setTextSize(size);

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/constanb.ttf");
        tPaint.setTypeface(font);
        textBounds = new Rect();
        tPaint.getTextBounds(QUOTE, 0, QUOTE.length(), textBounds);
        mTextWidth = Math.round(tPaint.measureText(QUOTE.toString())); // Use measureText to calculate width
        mTextHeight = textBounds.height(); // Use height from getTextBounds()
        mCirlcePaint = new Paint();
        mCirlcePaint.setStyle(Paint.Style.FILL);
        mCirlcePaint.setColor(Color.TRANSPARENT);
//
//        mCirlcePaint.setTextAlign(Paint.Align.CENTER);
//        radius = (float) ((mTextWidth) / (Math.PI));

//        if(getResources().getDisplayMetrics().xdpi>getResources().getDisplayMetrics().ydpi){
//            radius=getResources().getDisplayMetrics().xdpi/2;
//        }
//        else

        canvas.rotate(180, getWidth()/2 , getHeight()/2);
//38
        canvas.drawCircle(centerX, centerY, getResources().getDimension(R.dimen._40sdp), mCirlcePaint);
//        circle.addCircle(centerX, centerY, 70, Path.Direction.CCW);
        circle.addCircle(getWidth()/2, getHeight()/2, getResources().getDimension(R.dimen._28sdp), Path.Direction.CCW);
        //canvas.drawTextOnPath(QUOTE, circle,  getResources().getDimension(R.dimen._70sdp), 0, tPaint);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            canvas.drawTextOnPath(QUOTE, circle,  getResources().getDimension(R.dimen._34sdp), 0, tPaint);
        }

        else
            canvas.drawTextOnPath(QUOTE, circle,  getResources().getDimension(R.dimen._3sdp), 0, tPaint);



        super.onDraw(canvas);
        //worked
    }
    public void changeTextsize(Float f){
        this.size=f;
    }
}

