package com.app.signidapp.utils;

import android.content.Context;
import android.util.AttributeSet;

import com.app.signidapp.font.GothamMediumTextView;

public class BlueWhiteImageview extends GothamMediumTextView {
    public BlueWhiteImageview(Context context) {
        super(context);
    }

    public BlueWhiteImageview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BlueWhiteImageview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
