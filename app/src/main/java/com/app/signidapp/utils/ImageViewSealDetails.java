package com.app.signidapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.signidapp.font.ConstintinaBoldTextView;
import com.app.signidapp.font.GothamMediumTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ImageViewSealDetails extends GothamMediumTextView {
    private String QUOTE ="Neha karakotikjfkas";
    Float f=10f;
    private Path circle,circle2;
    private Paint mCirlcePaint;
    private Paint tPaint;
    private Rect textBounds;
    private int mTextWidth, mTextHeight, centerX, centerY;
    private float radius;
    public ImageViewSealDetails(Context context) {
        super(context);
    }

    public ImageViewSealDetails(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageViewSealDetails(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        circle = new Path();

        tPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        tPaint.setColor(Color.WHITE);
        tPaint.setTextSize(60f);

        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/constanb.ttf");
        tPaint.setTypeface(font);
//        tPaint.setTextAlign(Paint.Align.CENTER);
        textBounds = new Rect();
        tPaint.getTextBounds(QUOTE, 0, QUOTE.length(), textBounds);
        mTextWidth = Math.round(tPaint.measureText(QUOTE.toString())); // Use measureText to calculate width
        mTextHeight = textBounds.height(); // Use height from getTextBounds()

        mCirlcePaint = new Paint();
        mCirlcePaint.setStyle(Paint.Style.FILL);
        mCirlcePaint.setColor(Color.TRANSPARENT);
        if(QUOTE.length()<8){
            radius=200;
            canvas.rotate(360, getWidth()/4 , getHeight()/4);
            int x = (canvas.getWidth() / 2) - (textBounds.width() / 2);
            int y = (canvas.getHeight() / 2) - (textBounds.height() / 2);
            canvas.drawText(QUOTE, y, x, tPaint);
        }
        else if(QUOTE.length()<19){


            radius=160;
            canvas.rotate(180, getWidth()/4 , getHeight()/4);

            canvas.drawCircle(centerX, centerY, radius, mCirlcePaint);
            circle.addCircle(centerX, centerY, radius, Path.Direction.CW);
            canvas.drawTextOnPath(QUOTE, circle, 130, 0, tPaint);
        }
        else if(QUOTE.length()<25){

            radius=160;
            canvas.rotate(180, getWidth()/4 , getHeight()/4);

            canvas.drawCircle(centerX, centerY, radius, mCirlcePaint);
            circle.addCircle(centerX, centerY, radius, Path.Direction.CW);
            canvas.drawTextOnPath(QUOTE, circle, radius, 0, tPaint);
        }

        else{
            radius = (float) ((mTextWidth) / (Math.PI));
//radius 70 is workintg  fine
            canvas.rotate(180, getWidth()/4 , getHeight()/4);
            // canvas.rotate(180, getWidth() , getHeight());
            canvas.drawCircle(centerX, centerY, radius, mCirlcePaint);
            circle.addCircle(centerX, centerY, radius, Path.Direction.CW);
            canvas.drawTextOnPath(QUOTE, circle, radius, 0, tPaint);}
        //11070
        super.onDraw(canvas);
        //worked
    }
    public void changeText(String s){
        this.QUOTE=s;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

    }
}
