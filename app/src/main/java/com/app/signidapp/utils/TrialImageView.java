package com.app.signidapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;

import com.app.signidapp.R;
import com.app.signidapp.font.GothamMediumTextView;

import static com.facebook.FacebookSdk.getApplicationContext;

public class TrialImageView  extends GothamMediumTextView {
    private String QUOTE ="Neha karakotikjfkas";

    Context context;
    private Paint tPaint;
    public TrialImageView(Context context) {
        super(context);
        this.context=context;
    }

    public TrialImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TrialImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        tPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        tPaint.setColor(Color.BLACK);
        tPaint.setTextSize(60f);
        Rect frameToDraw = new Rect(0, 0, width, height);
        RectF whereToDraw = new RectF(0, 0, width, height);
        Typeface font = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/constanb.ttf");
        tPaint.setTypeface(font);
        tPaint.getTextBounds(QUOTE, 0, QUOTE.length(), frameToDraw);



        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.background_keyboard_activity);
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);

        canvas.drawBitmap(bitmap,frameToDraw,whereToDraw, tPaint);
    }
}
