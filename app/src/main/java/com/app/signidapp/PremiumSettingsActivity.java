package com.app.signidapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.signidapp.adapter.PremiumScreenAdapter;
import com.app.signidapp.model.SealInfo;
import com.app.signidapp.session.SignIDAppSession;
import com.app.signidapp.utils.CommonLoadingDialog;
import com.app.signidapp.utils.Content;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PremiumSettingsActivity extends AppCompatActivity {
    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    PremiumScreenAdapter pAdapter;
    SignIDAppSession session;
    List<SealInfo> sealList = new ArrayList<>();
    String s = "", s2 = "", semp2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium_settings);
        ButterKnife.bind(this);
   session = new SignIDAppSession(this);
        pAdapter = new PremiumScreenAdapter(sealList, PremiumSettingsActivity.this);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(PremiumSettingsActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        recyclerView.setAdapter(pAdapter);
    }

    public void sealedApi() {

        try {

            String url = Content.baseURL + "All_Seals_By_User_Id.php?page_no=1&per_page=2000&user_id=" + session.getUser_id();


            CommonLoadingDialog.showLoadingDialog(PremiumSettingsActivity.this, "Checking..");


            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            CommonLoadingDialog.closeLoadingDialog();


                            System.out.println("RESPONSE=====" + response);


                            try {
                                sealList.clear();
                                sealList.add(new SealInfo("", "", ""));
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("0")) {
                                    sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));
                                    sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));
                                    sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));
                                    sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/23.png", "Neha", "Dharmani"));
                                    pAdapter.notifyDataSetChanged();

                                }
                                if (!jsonObject.isNull("sealInfo")) {
                                    JSONArray mJsonArray = jsonObject.getJSONArray("sealInfo");
                                    if (mJsonArray.length() == 0) {
                                        sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "Neha Karakoti", "Chandigarh"));
                                        sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));
                                        sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));
                                        sealList.add(new SealInfo("http://signidapp.com/SignId/SealDocuments/22.png", "", ""));


                                    } else {
                                        for (int i = 0; i < mJsonArray.length(); i++) {
                                            semp2 = "";
                                            s = "";
                                            JSONObject mJsonData = mJsonArray.getJSONObject(i);
                                            if (!mJsonData.getString("town").equals("")) {
                                                if (!mJsonData.getString("town").equals(""))
                                                    if (mJsonData.getString("town").length() <= 19) {
                                                        int length = mJsonData.getString("town").length();

                                                        while (length < 25) {
//                                                            s = "   ".concat(mJsonData.getString("town"));
//                                                            s = s.concat("   ");

                                                            if (s.equals(""))
                                                                s = "  ".concat(mJsonData.getString("town"));
                                                            else
                                                                s = "  ".concat(s);
                                                            length++;
                                                        }
                                                    } else {
                                                        s = mJsonData.getString("town");
                                                    }
                                            }
                                            if (!mJsonData.getString("name").equals("")) {
                                                if (mJsonData.getString("name").length() <= 15) {
                                                    int length = mJsonData.getString("name").length();
                                                    if (length <= 8) {

                                                        while (length < 24) {

                                                            semp2 = "   ".concat(mJsonData.getString("name"));
                                                            semp2 = semp2.concat("   ");

                                                            length++;
                                                        }
                                                    } else {
                                                        while (length < 15) {

                                                            semp2 = "  ".concat(mJsonData.getString("name"));
                                                            semp2 = semp2.concat(" ");

                                                            length++;
                                                        }
                                                    }
                                                }
                                            }

//                                        for (int i = 0; i < mJsonArray.length(); i++) {
//                                            semp2 = "";
//                                            s = "";
//                                            JSONObject mJsonData = mJsonArray.getJSONObject(i);
//                                            if (!mJsonData.getString("town").equals("")) {
//                                                if (!mJsonData.getString("town").equals(""))
//                                                    if (mJsonData.getString("town").length() <= 19) {
//                                                        int length = mJsonData.getString("town").length();
//
//                                                        while (length < 9) {
//                                                            if (s.equals(""))
//                                                                s = "  ".concat(mJsonData.getString("town"));
//                                                            else
//                                                                s = "  ".concat(s);
//                                                            length++;
//                                                        }
//                                                    } else {
//                                                        s = mJsonData.getString("town");
//                                                    }
//                                            }
//                                            if (!mJsonData.getString("name").equals("")) {
//                                                if (mJsonData.getString("name").length() <= 15) {
//                                                    int length = mJsonData.getString("name").length();
//                                                    if (length <= 8) {
//
//                                                        while (length < 24) {
//
//                                                            semp2 = "   ".concat(mJsonData.getString("name"));
//                                                            semp2 = semp2.concat("   ");
//
//                                                            length++;
//                                                        }
//                                                    } else {
//                                                        while (length < 15) {
//
//                                                            semp2 = "  ".concat(mJsonData.getString("name"));
//                                                            semp2 = semp2.concat(" ");
//
//                                                            length++;
//                                                        }
//                                                    }
//                                                }
//                                            }

                                            sealList.add(new SealInfo(mJsonData.getString("image"), semp2,s));

                                        }
                                        pAdapter.notifyDataSetChanged();

                                    }
                                }

//


                            } catch (Exception ex) {
                                CommonLoadingDialog.closeLoadingDialog();
                                System.out.println("OUTPUT ======ERROR==" + ex);
                            }


                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            CommonLoadingDialog.closeLoadingDialog();
                            System.out.println("OUTPUT ======ERROR==" + error.toString());
                        }
                    });

            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PremiumSettingsActivity.this);
            stringRequest.setShouldCache(false);
            requestQueue.add(stringRequest);
        } catch (Exception ex) {
            System.out.println("OUTPUT ======ERROR==" + ex.toString());
        }

    }

    @OnClick(R.id.backBtn)
    public void onClick() {
        //startActivity(new Intent(PremiumSettingsActivity.this, StampTrialActivity.class));
        onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sealedApi();
    }
}
