package com.app.signidapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;

import com.app.signidapp.session.SignIDAppSession;

public class SettinActivity extends Activity {
    TextView btnSettings;
    boolean customlockpassword = false;
    boolean fingerPrint = true;
    SignIDAppSession signIDAppSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        btnSettings = findViewById(R.id.btnSettings);
        signIDAppSession = new SignIDAppSession(this);
        fingerPrint = getIntent().getBooleanExtra("fingerPrint", true);
        if (!fingerPrint) {
            customlockpassword = true;
            btnSettings.setText("Click here to enable the custom lock for SignID");
        }


        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (customlockpassword) {

                    Intent intent=new Intent(SettinActivity.this,SettingActivity.class);
                    finish();
                    startActivity(intent);


                } else {

                    finish();
                    startActivityForResult(new Intent(Settings.ACTION_SECURITY_SETTINGS), 0);
                }



            }
        });


    }


    public void passcodeSecurity(Context context) {

        Intent i = new Intent(getApplicationContext(), SampleActivity.class);
        if (signIDAppSession.getPasscode().equals("")) {
            i.putExtra("passcode", "");
        } else {
            i.putExtra("passcode", "alreadysetted");
        }


        finish();

        startActivity(i);
    }

}
