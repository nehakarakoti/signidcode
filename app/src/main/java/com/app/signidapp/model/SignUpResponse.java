package com.app.signidapp.model;

/**
 * Created by RITUPARNA on 6/11/2018.
 */

public class SignUpResponse
{
    private String message;

    private SignUpData Data;

    private String status;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public SignUpData getData ()
    {
        return Data;
    }

    public void setData (SignUpData Data)
    {
        this.Data = Data;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", Data = "+Data+", status = "+status+"]";
    }
}
