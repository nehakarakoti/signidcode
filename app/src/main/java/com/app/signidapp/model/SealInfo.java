package com.app.signidapp.model;

import java.io.Serializable;

public class SealInfo implements Serializable {

    private String id="";

    private String imageId="";

    private String image="";

    private String name="";

    private String town="";

    private String userId="";

    private String favSeal="";

    public SealInfo(String image, String name, String town) {


        this.image = image;
        this.name = name;
        this.town = town;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFavSeal() {
        return favSeal;
    }

    public void setFavSeal(String favSeal) {
        this.favSeal = favSeal;
    }
}
