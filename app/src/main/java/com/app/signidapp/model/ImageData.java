package com.app.signidapp.model;

public class ImageData {
    String url="";
    String image_ID="";
    boolean isChecked=false;

    public String getImage_ID() {
        return image_ID;
    }

    public void setImage_ID(String image_ID) {
        this.image_ID = image_ID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public ImageData(String url, boolean isChecked,String image_id) {
        this.url = url;
        this.isChecked = isChecked;
        this.image_ID=image_id;
    }
}
