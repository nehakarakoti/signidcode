package com.app.signidapp.model;

/**
 * Created by RITUPARNA on 6/12/2018.
 */

public class FbSignInResponse
{
    private String Message;

    private String status;

    private FbSignInData data;

    public String getMessage ()
    {
        return Message;
    }

    public void setMessage (String Message)
    {
        this.Message = Message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public FbSignInData getData ()
    {
        return data;
    }

    public void setData (FbSignInData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Message = "+Message+", status = "+status+", data = "+data+"]";
    }
}
