package com.app.signidapp.model;

/**
 * Created by RITUPARNA on 6/11/2018.
 */

public class SignInResponse
{
    private String message;

    private String status;

    private SignInData data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public SignInData getData ()
    {
        return data;
    }

    public void setData (SignInData data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }
}
