package com.app.signidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.app.signidapp.session.SignIDAppSession;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GoActivity extends AppCompatActivity {


    @BindView(R.id.text_got_it)
    TextView text_got_it;

    SignIDAppSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go);
        ButterKnife.bind(this);

        session=new SignIDAppSession(this);





        text_got_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                session.sethowGoFirstTime("no");
                startActivity(new Intent(GoActivity.this, SettingActivity.class));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        });
    }
}
