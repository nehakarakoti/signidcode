package com.app.signidapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.app.signidapp.session.SignIDAppSession;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;


public class SplashScreen extends AppCompatActivity {

    public static int SPLASH_TIME_OUT=3000;
    SignIDAppSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        setContentView(R.layout.activity_splash_screen);

        session=new SignIDAppSession(this);


        keyBoardListingCheck();



       // boolean hasSoftKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();






        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                /*This block is for very first time*/
                if(session.getKeyShowFirstTime().equals(""))
                {
                    startActivity(new Intent(SplashScreen.this, MainActivity.class));
                    finish();
                }
                else
                {

                     /*This block is for if keyboard is not listed yet*/
                      if( session.getKeyShowSetting().equals(""))
                      {
                            startActivity(new Intent(SplashScreen.this, KeyboardSettingActivity.class));
                          overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();
                      }
                      /*this block of code is executed while keyoard is listed */
                      else
                      {
                         /*This block of code is executed while keyboard is listed n choosed too*/
                        if(Settings.Secure.getString(getContentResolver(),Settings.Secure.DEFAULT_INPUT_METHOD).equals("com.app.signidapp/.utils.SoftKeyboard"))
                        {
                          startActivity(new Intent(SplashScreen.this,SettingActivity.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                          finish();
                        }
                         /*This block of code is executed while keyboard is listed but not choosed */
                        else
                        {
                            startActivity(new Intent(SplashScreen.this,KeyboardChoosing.class));
                            overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                            finish();
                        }
                      }

                     /*This block is for keyboard is listed but not choosed*/
                }





                // close this activity

            }
        }, SPLASH_TIME_OUT);
    }

    private void keyBoardListingCheck()
    {
        InputMethodManager imeManager = (InputMethodManager) getApplicationContext().getSystemService(INPUT_METHOD_SERVICE);
        for (int i=0;i<imeManager.getEnabledInputMethodList().size();i++)
        {
            if( imeManager.getEnabledInputMethodList().get(i).getPackageName().equals("com.app.signidapp"))
            {
                session.setKeySetting("yes");
                break;

            }
            else
            {
                session.setKeySetting("");
            }
        }
    }



    }

