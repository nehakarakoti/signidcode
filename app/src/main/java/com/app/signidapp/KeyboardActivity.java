package com.app.signidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.signidapp.session.SignIDAppSession;

import butterknife.BindView;
import butterknife.ButterKnife;

public class KeyboardActivity extends AppCompatActivity {
//@BindView(R.id.text_registerNow)
//TextView tvRegisterNow;
    @BindView(R.id.backBtn)
    ImageView backBtn;
    SignIDAppSession signIDAppSession;
    @BindView(R.id.text_registerNow)
    TextView text_registerNow;
    com.app.signidapp.font.MyriadProTextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_keyboard);


        ButterKnife.bind(this);
        textView=findViewById(R.id.text_registerNow);
        signIDAppSession=new SignIDAppSession(KeyboardActivity.this);
        if(signIDAppSession!=null){
            if(signIDAppSession.getUser_name()!=null){
                if(!signIDAppSession.getUser_name().equals("")){
                    textView.setVisibility(View.GONE);
                }
            }
        }
//       if( signIDAppSession.getUser_email().equals("")){
//           tvRegisterNow.setVisibility(View.GONE);
//       }
//       else {
//           tvRegisterNow.setVisibility(View.VISIBLE);
//       }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 onBackPressed();

            }
        });

        text_registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(KeyboardActivity.this,ContinueActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
                finish();
            }
        });

    }
}
