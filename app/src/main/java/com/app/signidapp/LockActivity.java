package com.app.signidapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.signidapp.session.SignIDAppSession;

public class LockActivity extends AppCompatActivity {
    public static int pressed = 0;
    TextView tvTittle;
    ImageView ivBack;
    Button btnSubmit;
    View vOld;
    SignIDAppSession signIDAppSession;
    RelativeLayout rlOldPasscode;
    EditText etConfirmPasscode, etTrial, etEnterPasscode, etEnterOldPasscode;
    String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);
        etTrial = findViewById(R.id.etTrial);
        rlOldPasscode = findViewById(R.id.rlOldPasscode);
        vOld = findViewById(R.id.vOld);
        etEnterOldPasscode = findViewById(R.id.etEnterOldPasscode);
        ivBack = findViewById(R.id.ivBack);
        signIDAppSession = new SignIDAppSession(LockActivity.this);
        btnSubmit = findViewById(R.id.btnSubmit);
        etConfirmPasscode = findViewById(R.id.etConfirmPasscode);
        etEnterPasscode = findViewById(R.id.etEnterPasscode);
        tvTittle = findViewById(R.id.tvTittle);

        etEnterOldPasscode.setImeOptions(EditorInfo.IME_ACTION_DONE);

        etEnterPasscode.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etConfirmPasscode.setImeOptions(EditorInfo.IME_ACTION_DONE);
//etConfirmPasscode.addTextChangedListener(new TextWatcher() {
//    @Override
//    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//    }
//
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        if (start == 3) {
//            InputFilter[] filterArray = new InputFilter[1];
//            filterArray[0] = new InputFilter.LengthFilter(4);
//            etConfirmPasscode.setFilters(filterArray);
//            etConfirmPasscode.clearFocus();
//         //   Toast.makeText(LockActivity.this, "you need to set only 4 digits character", Toast.LENGTH_SHORT).show();
////                  etTrial.requestFocus();
//etTrial.requestFocus();
//        }
//
//        else {
//
//            InputFilter[] filterArray = new InputFilter[1];
//            filterArray[0] = new InputFilter.LengthFilter(20);
//            etEnterPasscode.setFilters(filterArray);
//
//        }
//    }
//
//    @Override
//    public void afterTextChanged(Editable s) {
//
//    }
//});

        etEnterPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start >= 3) {

                    if (before != 1) {
                        InputFilter[] filterArray = new InputFilter[1];
                        filterArray[0] = new InputFilter.LengthFilter(4);
                        etEnterPasscode.setFilters(filterArray);
                        etEnterPasscode.clearFocus();
                        //  Toast.makeText(LockActivity.this, "you need to set only 4 digits character", Toast.LENGTH_SHORT).show();
                        etConfirmPasscode.requestFocus();
                    }
                } else {

                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(20);
                    etEnterPasscode.setFilters(filterArray);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        etEnterOldPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (start == 3) {
                    if (before != 1) {
                        InputFilter[] filterArray = new InputFilter[1];
                        filterArray[0] = new InputFilter.LengthFilter(4);
                        etEnterOldPasscode.setFilters(filterArray);
                        etEnterOldPasscode.clearFocus();
                        etEnterPasscode.requestFocus();
                    }
                } else {

                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(20);
                    etEnterOldPasscode.setFilters(filterArray);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etConfirmPasscode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (start == 3) {
                    if (before != 1) {
                        InputFilter[] filterArray = new InputFilter[1];
                        filterArray[0] = new InputFilter.LengthFilter(4);
                        etConfirmPasscode.setFilters(filterArray);
                        etConfirmPasscode.clearFocus();
                        etTrial.requestFocus();
                    }
                } else {

                    InputFilter[] filterArray = new InputFilter[1];
                    filterArray[0] = new InputFilter.LengthFilter(20);
                    etConfirmPasscode.setFilters(filterArray);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if (!signIDAppSession.getPasscode().equals("")) {
            tvTittle.setText(getString(R.string.string_changePasscode));
            rlOldPasscode.setVisibility(View.VISIBLE);
            etEnterPasscode.setHint(getString(R.string.string_newPasscode));
            vOld.setVisibility(View.VISIBLE);

        } else {
            rlOldPasscode.setVisibility(View.GONE);
            vOld.setVisibility(View.GONE);
            tvTittle.setText(getString(R.string.string_createPasscode));
        }


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rlOldPasscode.getVisibility() == View.VISIBLE) {
                    if (etEnterOldPasscode.getText().toString().trim().equals("")) {
                        Toast.makeText(LockActivity.this, getString(R.string.currentPasscode), Toast.LENGTH_SHORT).show();

                    } else {
                        if (etEnterPasscode.getText().toString().trim().equals("")) {
                            Toast.makeText(LockActivity.this, getString(R.string.enterPasscode), Toast.LENGTH_SHORT).show();
                        } else if (etConfirmPasscode.getText().toString().trim().equals("")) {
                            Toast.makeText(LockActivity.this, getString(R.string.confirmPasscode), Toast.LENGTH_SHORT).show();

                        } else if (etEnterPasscode.getText().toString().trim().length() < 4) {
                            Toast.makeText(LockActivity.this, getString(R.string.enter4Char), Toast.LENGTH_SHORT).show();

                        } else if (!etEnterPasscode.getText().toString().trim().equals(etConfirmPasscode.getText().toString().trim())) {
                            Toast.makeText(LockActivity.this, getString(R.string.matchwithconfirmedpasscode), Toast.LENGTH_SHORT).show();
                            etConfirmPasscode.setText("");
                        } else if (!etEnterOldPasscode.getText().toString().trim().equals(signIDAppSession.getPasscode())) {
                            Toast.makeText(LockActivity.this, getString(R.string.enterPasscodeCorrectly), Toast.LENGTH_SHORT).show();
                            etEnterOldPasscode.setText("");
                        } else {
                            Toast.makeText(LockActivity.this, getString(R.string.passwordsetted), Toast.LENGTH_SHORT).show();

                            password = etConfirmPasscode.getText().toString().trim();
                            signIDAppSession.setPasscode(password);

                            finish();
                            SettingActivity.pressedbutton = false;
//         onBackPressed();
                        }
                    }

                } else {


                    if (etEnterPasscode.getText().toString().trim().equals("")) {
                        Toast.makeText(LockActivity.this, getString(R.string.enterPasscode), Toast.LENGTH_SHORT).show();
                    } else if (etConfirmPasscode.getText().toString().trim().equals("")) {
                        Toast.makeText(LockActivity.this, getString(R.string.confirmPasscode), Toast.LENGTH_SHORT).show();
                    } else if (etEnterPasscode.getText().toString().trim().length() < 4) {
                        Toast.makeText(LockActivity.this, getString(R.string.enter4Char), Toast.LENGTH_SHORT).show();
                    } else if (!etEnterPasscode.getText().toString().trim().equals(etConfirmPasscode.getText().toString().trim())) {
                        Toast.makeText(LockActivity.this, getString(R.string.matchwithconfirmedpasscode), Toast.LENGTH_SHORT).show();
                        etConfirmPasscode.setText("");
                    } else {
                        Toast.makeText(LockActivity.this, getString(R.string.passwordsetted), Toast.LENGTH_SHORT).show();

                        password = etConfirmPasscode.getText().toString().trim();
                        signIDAppSession.setPasscode(password);

                        SettingActivity.pressedbutton = false;
                        finish();

                    }
                }
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressed = 1;
                onBackPressed();

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

//    public void changeTypeface(EditText e) {
//        Typeface existingTypeface = e.getTypeface();
//        e.setInputType((InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD));
//
//        e.setTypeface(existingTypeface);
//    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

}
