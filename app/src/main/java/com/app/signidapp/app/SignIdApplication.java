package com.app.signidapp.app;

import android.app.Application;
import android.app.ProgressDialog;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class SignIdApplication extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

    }

 
}
