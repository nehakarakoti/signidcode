package com.app.signidapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.app.signidapp.model.SealInfo;
import com.app.signidapp.utils.ImageViewSealDetails;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SealDetailActivity extends AppCompatActivity {
    String url = "";
    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.ivName)
    ImageViewSealDetails ivName;
    @BindView(R.id.ivSeal)
    ImageView ivSeal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seal_detail);
        ButterKnife.bind(this);

        gettingData();
    }

    private void gettingData() {
        SealInfo sealInfo = (SealInfo) getIntent().getSerializableExtra("SealInfo");
        if (sealInfo != null) {
            Glide.with(this).load(
                    sealInfo.getImage())
                    .into(ivSeal);

            ivName.changeText(sealInfo.getName());
        }
    }

    @OnClick(R.id.backBtn)
    public void onClick() {
        finish();
    }
}
